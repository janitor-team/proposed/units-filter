Units-filter
############

:author: Georges Khaznadar <georgesk@debian.org>
:copyright: 2000-2020
:licence: GPL v2+

Units-filter est un filtre simple et autonome écrit en langage C,
flex et bison. Il accepte des entrées telles que `"1.5e3 µN.m.s^-1"` 
(Ce pourrait être le taux de croissance temporel d'un couple) et
et sort une valeur en unité SI, suivie de la dimension physique de
cette valeur.

.. code-block::

    example:~/src$ echo 1.5e3 µN.m.s^-1 | units-filter
    0.0015   2   1  -3   0   0   0   0
    example:~/src$

ce qui signifie : |formula1|

Quand la chaîne d'entrée n'est pas reconnue, le filtre échoue et 
renvoie un code `1`.

Ce filtre peut être utilisé au sein de systèmes d'examens éducatifs,
pour analyser la réponse d'un étudiant à un problème de physique ou
de chimie.

Units-filter a de nombreuses propriétés, telles que :

- préférer des unités usuelles, plutôt que les unités SI brutes
- fabriquer du code LaTeX
- considérer la précision (chiffres significatifs en nombre donné)

Les suggestions sont bienvenues.

INSTALLATION
============

Dans le sous-répertoire src/, lancez "make".

Vous pouvez essayer le script units-test pour voir quelques entrées
typiques et leurs résultats.

.. |formula1| image:: formula1.png
    :alt: 0.0015 (SI unit) m^2 .kg^{-3}.s	     

