Guide de l'utilisateur de Units-Master
######################################

units-master est un programme qui permet d'entrer une expression de
quantité physique ou chimique, par exemple 1.8 km/h ou 3.2 µmol/L, et
de réaliser quelques opérations avec : vérifier la syntaxe de
l'expression, réaliser des conversions, par exemple convertir 1.8 km/h
en mètres par seconde, ce qui donne 0.5 m/s, et d'obtenir le rendu de
cette expression dans divers formats : LaTeX (ordinaire), PNG et SVG.

D'abord : saisir une quantité
=============================

Dans la section "ENTRÉE", on entre une quantité : généralement une
valeur numérique suivie par une unité physique ou chimique. Des
expressions comme "1.8 km/h" ou "5h 5min 32s" sont permises ; quand on
compose des unités, on utilise un point, comme dans l'unité de moment
de torsion "N.m" ; les accélérations peuvent s'exprimer avec unités
comme "m.s^-2", ou "m/s/s".

Vérification de la syntaxe
==========================

Le bouton sous la ligne de saisie principale permet de vérifier
l'expression.  Si l'expression est correcte, la ligne en dessous
affichera une expression avec un polynôme basé sur les unités S.I. ;
par exemple, "1 N" donnera "1 m.kg.s^-2", comme une force est le
produit d'une masse par une accélération.

S'il y a une erreur, cette même ligne présentera un message en
relation avec celle-ci.

Convertir en ...
================

Le menu combiné étiqueté "Unité" demande une nouvelle unité. Par
exemple, si on tape (et qu'on valide) "kN", et que l'expression était
précédemment "1000 N", cette dernière sera remplacée par "1.OOO kN",
et on verra un message de succès sous le menu. Si l'unité demandée
n'est pas compatible avec l'unité précédemment entrée, c'est un
message d'erreur qui sera affiché au-dessous.

Rendu d'une expression
======================

Le menu en bas à droite de la fenêtre permet de choisir un format de
rendu.  Les formats couramment supportés sont LaTeX, PNG and SVG. Le
rendu commence quand on change le choix du format.
