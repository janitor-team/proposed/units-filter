Units-Master
############

Units-master is a graphic program that allows one to manage expressions of
physical or chemical quantities.

Author
======

* © 2021 Georges Khaznadar <georgesk@debian.org>

License
=======

Units-master is a free software: one can copy it, view its source, modify it,
and distribute it in original or modified form. The complete license is
GNU General Public License version 3.

On Debian systems, this license can be found at
`/usr/share/common-licenses/GPL-3 <file:///usr/share/common-licenses/GPL-3>`_.

