Units-filter
############

:author: Georges Khaznadar <georgesk@debian.org>
:copyright: 2000-2020
:licence: GPL v2+

Units-filter is a basic standalone filter written in C language,
flex and bison. It inputs strings like `"1.5e3 µN.m.s^-1"` (it could
be the time growth ratio of a torque) and outputs the value in
standard SI unit, followed by the physical dimension of this value.

.. code-block::

    example:~/src$ echo 1.5e3 µN.m.s^-1 | units-filter
    0.0015   2   1  -3   0   0   0   0
    example:~/src$

    
which means : |formula1|

When the input is not successfully parsed, the filter fails with
the exit code `1`.

This filter can be embedded in educational test systems, in order to
analyze a student's answer to a problem of physics or chemistry.

Unit-filter has many features, like:

- prefer usual units, rather than raw SI units
- output LaTeX code
- take precision in account (fit to a given number of significant digits)

Feedback is welcome.


INSTALLATION
============

inside the src/ subdirectory, run "make".

You can try the script `units-test` to see some typical inputs and
outputs. 

.. |formula1| image:: formula1.png
    :alt: 0.0015 (SI unit) m^2 .kg^{-3}.s	     

